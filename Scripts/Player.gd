extends Area2D

var screenSize
export var speed = 1000

onready var bullet = preload("res://Scene/Laser.tscn")

func _ready():
	screenSize = get_viewport_rect().size
	pass

func _process(delta):
	var velocity_x = 0
	
	if Input.is_action_pressed("ui_right"):
		velocity_x = 1
	if Input.is_action_pressed("ui_left"):
		velocity_x = -1
		
	position.x += velocity_x * speed * delta
	position.x = clamp(position.x, 0 , screenSize.x)
	
	if Input.is_action_just_pressed("ui_select"):
		var n_bullet = bullet.instance()
		n_bullet.position = position
		n_bullet.position.y -= 30
		get_node("/root/Main").add_child(n_bullet)