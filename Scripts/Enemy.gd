extends Area2D

var screenSize
export var speed = 100
export var forwardStep = 30
var direction


func _ready():
	screenSize = get_viewport_rect().size
	direction = "left"
	pass

func _process(delta):
	var velocity = 0
	if direction == "left":
		velocity = -1
	else :
		velocity = 1
	position.x += velocity * speed * delta
	
	if position.x < -50 or position.x > screenSize.x + 50:
		position.y += forwardStep
		if direction == "left":
			direction = "right"
		else:
			direction = "left"
	pass
